var gulp = require('gulp');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('gulp-browserify');

gulp.task('css', function () {
    return gulp.src('src/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 0.25%'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(gulp.dest('build/css'));
});

gulp.task('css:w', function () {
    gulp.watch('src/sass/**/*.scss', ['css']);
});

gulp.task('js', function () {
    return gulp.src('src/js/index.js')
    .pipe(browserify())
    .pipe(gulp.dest('build/js'));
});

gulp.task('js:w', function() {
    gulp.watch('src/js/**/*.js', ['js']);
});

gulp.task('html', function () {
    return gulp.src('src/index.html')
        .pipe(replace('/build/', ''))
        .pipe(gulp.dest('build'));
});

gulp.task('assets', function () {
    return gulp.src('src/fonts/*.*')
        .pipe(gulp.dest('build/fonts'));
});

gulp.task('build', ['css', 'html', 'assets'], function (done) {
    return done();
});

gulp.task('watch', function () {
    gulp.watch('src/index.html', ['html']);
    gulp.watch('src/sass/**/*.scss', ['css']);
    gulp.watch('src/js/**/*.js', ['js']);
});

gulp.task('default', function () {
    gulp.run('build');
});
