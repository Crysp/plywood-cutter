var Marks = require('../collections/marks');
var Kinds = require('../collections/kinds');
var WPS = require('../collections/wps');
var Formats = require('../collections/formats');
var Depths = require('../collections/depths');

var marksStatic = new Marks([
    { type: 'fanera_fk' },
    { type: 'fanera_fsf' },
    { type: 'fanera_fbs' },
    { type: 'fanera_lam' }
]);
//var kinds = new Kinds([
//    { id: 1, name: '1/2' },
//    { id: 2, name: '2/2' },
//    { id: 3, name: '2/3' },
//    { id: 4, name: '2/4' },
//    { id: 5, name: '3/3' },
//    { id: 6, name: '3/4' },
//    { id: 7, name: '4/4' },
//    { id: 8, name: 'Каркасная ТУ' }
//]);
var wps = new WPS([
    { format: '1525x1525', depth: 3, weight: 5, price: 2 },
    { format: '1525x1525', depth: 4, weight: 6, price: 2 },
    { format: '1525x1525', depth: 5, weight: 8, price: 3 },
    { format: '1525x1525', depth: 6, weight: 9, price: 4 },
    { format: '1525x1525', depth: 8, weight: 12, price: 5 },
    { format: '1525x1525', depth: 9, weight: 14, price: 5 },
    { format: '1525x1525', depth: 10, weight: 15, price: 6 },
    { format: '1525x1525', depth: 12, weight: 18, price: 7 },
    { format: '1525x1525', depth: 14, weight: 21, price: 8 },
    { format: '1525x1525', depth: 15, weight: 23, price: 9 },
    { format: '1525x1525', depth: 16, weight: 24, price: 10 },
    { format: '1525x1525', depth: 18, weight: 27, price: 11 },
    { format: '1525x1525', depth: 20, weight: 30, price: 12 },
    { format: '1525x1525', depth: 21, weight: 32, price: 13 },
    { format: '1525x1525', depth: 24, weight: 36, price: 14 },
    { format: '1525x1525', depth: 25, weight: 38, price: 15 },
    { format: '1525x1525', depth: 27, weight: 41, price: 16 },
    { format: '1525x1525', depth: 30, weight: 45, price: 18 },
    { format: '1525x1525', depth: 35, weight: 53, price: 21 },
    { format: '1525x1525', depth: 40, weight: 60, price: 24 },

    { format: '1220x2440', depth: 3, weight: 6, price: 2 },
    { format: '1220x2440', depth: 4, weight: 8, price: 3 },
    { format: '1220x2440', depth: 5, weight: 10, price: 3 },
    { format: '1220x2440', depth: 6, weight: 12, price: 4 },
    { format: '1220x2440', depth: 8, weight: 15, price: 5 },
    { format: '1220x2440', depth: 9, weight: 17, price: 6 },
    { format: '1220x2440', depth: 10, weight: 19, price: 6 },
    { format: '1220x2440', depth: 12, weight: 23, price: 8 },
    { format: '1220x2440', depth: 14, weight: 27, price: 9 },
    { format: '1220x2440', depth: 15, weight: 29, price: 9 },
    { format: '1220x2440', depth: 16, weight: 31, price: 10 },
    { format: '1220x2440', depth: 18, weight: 35, price: 11 },
    { format: '1220x2440', depth: 20, weight: 39, price: 13 },
    { format: '1220x2440', depth: 21, weight: 41, price: 13 },
    { format: '1220x2440', depth: 24, weight: 46, price: 15 },
    { format: '1220x2440', depth: 25, weight: 48, price: 16 },
    { format: '1220x2440', depth: 27, weight: 52, price: 17 },
    { format: '1220x2440', depth: 30, weight: 58, price: 19 },
    { format: '1220x2440', depth: 35, weight: 68, price: 22 },
    { format: '1220x2440', depth: 40, weight: 77, price: 25 },

    { format: '1250x2500', depth: 3, weight: 6, price: 3 },
    { format: '1250x2500', depth: 4, weight: 8, price: 4 },
    { format: '1250x2500', depth: 5, weight: 10, price: 5 },
    { format: '1250x2500', depth: 6, weight: 12, price: 5 },
    { format: '1250x2500', depth: 8, weight: 16, price: 7 },
    { format: '1250x2500', depth: 9, weight: 17, price: 6 },
    { format: '1250x2500', depth: 10, weight: 20, price: 9 },
    { format: '1250x2500', depth: 12, weight: 24, price: 11 },
    { format: '1250x2500', depth: 14, weight: 28, price: 13 },
    { format: '1250x2500', depth: 15, weight: 30, price: 14 },
    { format: '1250x2500', depth: 16, weight: 33, price: 14 },
    { format: '1250x2500', depth: 18, weight: 37, price: 16 },
    { format: '1250x2500', depth: 20, weight: 41, price: 18 },
    { format: '1250x2500', depth: 21, weight: 43, price: 19 },
    { format: '1250x2500', depth: 24, weight: 49, price: 22 },
    { format: '1250x2500', depth: 25, weight: 51, price: 23 },
    { format: '1250x2500', depth: 27, weight: 55, price: 24 },
    { format: '1250x2500', depth: 30, weight: 61, price: 27 },
    { format: '1250x2500', depth: 35, weight: 71, price: 32 },
    { format: '1250x2500', depth: 40, weight: 81, price: 36 },

    { format: '1500x3000', depth: 3, weight: 9, price: 3 },
    { format: '1500x3000', depth: 4, weight: 12, price: 4 },
    { format: '1500x3000', depth: 5, weight: 15, price: 5 },
    { format: '1500x3000', depth: 6, weight: 18, price: 6 },
    { format: '1500x3000', depth: 8, weight: 23, price: 7 },
    { format: '1500x3000', depth: 9, weight: 26, price: 8 },
    { format: '1500x3000', depth: 10, weight: 29, price: 9 },
    { format: '1500x3000', depth: 12, weight: 35, price: 11 },
    { format: '1500x3000', depth: 14, weight: 41, price: 13 },
    { format: '1500x3000', depth: 15, weight: 44, price: 14 },
    { format: '1500x3000', depth: 16, weight: 47, price: 15 },
    { format: '1500x3000', depth: 18, weight: 53, price: 17 },
    { format: '1500x3000', depth: 20, weight: 59, price: 19 },
    { format: '1500x3000', depth: 21, weight: 61, price: 20 },
    { format: '1500x3000', depth: 24, weight: 70, price: 22 },
    { format: '1500x3000', depth: 25, weight: 73, price: 23 },
    { format: '1500x3000', depth: 27, weight: 79, price: 25 },
    { format: '1500x3000', depth: 30, weight: 88, price: 28 },
    { format: '1500x3000', depth: 35, weight: 102, price: 33 },
    { format: '1500x3000', depth: 40, weight: 117, price: 37 },

    { format: '1525x3050', depth: 3, weight: 9, price: 3 },
    { format: '1525x3050', depth: 4, weight: 12, price: 4 },
    { format: '1525x3050', depth: 5, weight: 15, price: 5 },
    { format: '1525x3050', depth: 6, weight: 18, price: 6 },
    { format: '1525x3050', depth: 8, weight: 24, price: 7 },
    { format: '1525x3050', depth: 9, weight: 27, price: 8 },
    { format: '1525x3050', depth: 10, weight: 30, price: 9 },
    { format: '1525x3050', depth: 12, weight: 35, price: 11 },
    { format: '1525x3050', depth: 14, weight: 42, price: 13 },
    { format: '1525x3050', depth: 15, weight: 45, price: 14 },
    { format: '1525x3050', depth: 16, weight: 48, price: 15 },
    { format: '1525x3050', depth: 18, weight: 54, price: 17 },
    { format: '1525x3050', depth: 20, weight: 60, price: 19 },
    { format: '1525x3050', depth: 21, weight: 63, price: 20 },
    { format: '1525x3050', depth: 24, weight: 73, price: 22 },
    { format: '1525x3050', depth: 25, weight: 76, price: 23 },
    { format: '1525x3050', depth: 27, weight: 82, price: 25 },
    { format: '1525x3050', depth: 30, weight: 91, price: 28 },
    { format: '1525x3050', depth: 35, weight: 106, price: 33 },
    { format: '1525x3050', depth: 40, weight: 121, price: 37 },
]);
//
//marks.findWhere({ name: 'Фанера ФК' }).formats.add([
//    { id: 5, width: 1525, height: 1525 }
//]);
//
marksStatic.get('fanera_fk').depths.add([
    { value: 3, price: [8, 6, 4] },
    { value: 4, price: [10, 7, 5] },
    { value: 6, price: [15, 8, 7] },
    { value: 8, price: [20, 9, 9] },
    { value: 9, price: [23, 10, 10] },
    { value: 10, price: [25, 11, 11] },
    { value: 12, price: [38, 12, 11] },
    { value: 15, price: [38, 12, 11] },
    { value: 18, price: [53, 24, 22] },
    { value: 20, price: [53, 24, 22] },
    { value: 21, price: [53, 24, 22] }
]);
//
//marks.findWhere({ name: 'Фанера ФСФ' }).formats.add([
//    { id: 1, width: 1220, height: 2440 },
//    { id: 2, width: 1250, height: 2500 },
//    { id: 3, width: 1500, height: 3000 },
//    { id: 4, width: 1525, height: 3050 }
//]);
//
marksStatic.get('fanera_fsf').depths.add([
    { value: 4, price: [10, 7, 5] },
    { value: 6, price: [15, 8, 7] },
    { value: 9, price: [23, 10, 10] },
    { value: 12, price: [38, 12, 11] },
    { value: 15, price: [38, 12, 11] },
    { value: 18, price: [53, 24, 22] },
    { value: 21, price: [53, 24, 22] },
    { value: 24, price: [53, 24, 22] },
    { value: 27, price: [53, 24, 22] },
    { value: 30, price: [75, 34, 31] },
    { value: 35, price: [88, 40, 36] },
    { value: 40, price: [100, 45, 41] }
]);
//
//marks.findWhere({ name: 'Фанера ФБС-1' }).formats.add([
//    { id: 5, width: 1525, height: 1525 }
//]);
//
marksStatic.get('fanera_fbs').depths.add([
    { value: 10, price: [25, 11, 11] },
    { value: 12, price: [38, 12, 11] },
    { value: 14, price: [38, 12, 11] },
    { value: 15, price: [38, 12, 11] },
    { value: 16, price: [38, 12, 11] },
    { value: 18, price: [53, 24, 22] },
    { value: 20, price: [53, 24, 22] }
]);
marksStatic.get('fanera_lam').depths.add([
    { value: 3, price: [8, 6, 4] },
    { value: 4, price: [10, 7, 5] },
    { value: 6, price: [15, 8, 7] },
    { value: 8, price: [20, 9, 9] },
    { value: 9, price: [23, 10, 10] },
    { value: 10, price: [25, 11, 11] },
    { value: 12, price: [38, 12, 11] },
    { value: 15, price: [38, 12, 11] },
    { value: 18, price: [53, 24, 22] },
    { value: 20, price: [53, 24, 22] },
    { value: 21, price: [53, 24, 22] }
]);
//
//var _Plywood = Backbone.Model.extend({
//
//    idAttribute: 'type',
//
//    defaults: {
//        type: '',
//        name: ''
//    },
//
//    url: function () {
//        return 'http://www.geliostd.ru/api/getfilter?type=' + this.get('type');
//    },
//
//    parse: function (res) {
//        this.formats.add(res.formats);
//        for (var i = 0; i < res.depths.length; i++) {
//            this.depths.add({ value: Number(res.depths[i]) });
//        }
//        for (var j = 0; j < res.kinds.length; j++) {
//            this.kinds.add({ name: res.kinds[j] });
//        }
//        return res;
//    },
//
//    initialize: function () {
//        this.formats = new Formats;
//        this.depths = new Depths;
//        this.marks = new Marks;
//        this.kinds = new Kinds;
//    }
//
//});

//var _plywood = new _Plywood({ type: 'fanera_fk' });
//_plywood.fetch({
//    success: function () {
//        console.log(_plywood);
//    }
//});

//$.ajax({
//    method: 'get',
//    url: 'http://www.geliostd.ru/api/getfilter?type=fanera_fk',
//    success: function (res) {
//        console.log(res);
//    }
//});

var marks = new Marks([
    { type: 'fanera_fk' },
    { type: 'fanera_fsf' },
    { type: 'fanera_fbs' },
    { type: 'fanera_lam' }
]);

var _synced = false;
var synced = function () {
    return _synced;
};
var _syncedItems = 0;

for (var i = 0; i < marks.length; i++) {
    marks.models[i].fetch({
        success: function () {
            _syncedItems++;
            if (_syncedItems === marks.length) {
                _synced = true;
                Backbone.Events.trigger('PlywoodHelper:ready');
            }
        }
    });
}

module.exports = {
    marks: marks,
    marksStatic: marksStatic,
    wps: wps,
    synced: synced
};
