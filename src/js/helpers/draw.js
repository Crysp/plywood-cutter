var PlywoodHelper = require('./plywood');

var Plywoods = function (plywoods) {
    var structure = [];
    plywoods.each(function (plywood) {
        structure = structure.concat(Figures2(plywood));
    });

    return structure;
};

var Figures = function (plywood) {
    var plywoodWidth = plywood.getWidth();
    var plywoodHeight = plywood.getHeight();
    var leftLine = 0;
    var topLine = 0;
    var bottomLine = 0;
    var mark = PlywoodHelper.marks.findWhere({ id: plywood.get('mark') });
    var format = mark.formats.findWhere({ id: plywood.get('format') }) || mark.formats.models[0];
    var item = {
        width: plywoodWidth,
        height: plywoodHeight,
        figures: []
    };
    var structure = [item];
    plywood.figures.each(function (figure) {
        for (var i = 0; i < figure.get('count'); i++) {
            var width = figure.get('width');
            var height = figure.get('height');
            // new line
            if (leftLine + width > plywoodWidth && leftLine + height > plywoodWidth) {
                leftLine = 0;
                topLine = bottomLine;
            }
            // если перевернутую фигуру можно поместить на текущей линии, то надо ее перевернуть
            if (leftLine + width > plywoodWidth && leftLine + height < plywoodWidth) {
                var tmp = width;
                width = height;
                height = tmp;
            }
            // new canvas
            if (topLine + height > plywoodHeight && topLine + width > plywoodHeight) {
                item = {
                    width: plywoodWidth,
                    height: plywoodHeight,
                    figures: []
                };
                structure.push(item);
                topLine = 0;
                bottomLine = 0;
            }
            // если перевернутую фигуру можно поместить на текущем листе, то надо ее перевернуть
            if (topLine + height > plywoodHeight && topLine + width < plywoodHeight) {
                var tmp = width;
                width = height;
                height = tmp;
            }
            item.figures.push({
                width: width,
                height: height,
                x: leftLine,
                y: topLine
            });
            if (topLine + height > bottomLine) bottomLine = topLine + height;
            leftLine += width;
        }
    });

    return structure;
};

var Cuts = function (plywood) {
    var cuts = [];
    var _rightCuts = [];
    var _bottomCuts = [];
    for (var j = 0; j < plywood.sections.length; j++) {
        var section = plywood.sections[j];
        // right lines
        if (section.figure && (section.x + section.figure.width) != plywood.width) {
            _rightCuts.push({
                x1: section.x + section.figure.width,
                y1: section.y,
                x2: section.x + section.figure.width,
                y2: section.y + section.height
            });
        }
        // bottom lines
        if (section.figure && (section.y + section.figure.height) != plywood.height) {
            _bottomCuts.push({
                x1: section.x,
                y1: section.y + section.figure.height,
                x2: section.x + section.width,
                y2: section.y + section.figure.height
            });
        }
    }
    var intersection = function (lineA, lineB) {
        var intrs = null;
        if (lineA.x1 > lineB.x1 && lineA.x1 < lineB.x2 && lineA.y1 < lineB.y1 && lineA.y2 > lineB.y1) {
            intrs = {
                x: lineA.x1,
                y: lineB.y1
            };
        }
        return intrs;
    };
    // поиск пересечений
    for (var i = 0; i < _rightCuts.length; i++) {
        var cut = _rightCuts[i];
        for (var j = 0; j < _bottomCuts.length; j++) {
            var point = intersection(cut, _bottomCuts[j]);
            if (point) {
                cut.x2 = point.x;
                cut.y2 = point.y;
            }
        }
    }
    var connectionY = function (line) {
        var point = null;
        for (var i = 0; i < _rightCuts.length; i++) {
            var right = _rightCuts[i];
            if (line.x1 == right.x1 && line.y2 >= right.y1 && line.y2 <= right.y2 && line != right && !line.connected) {
                point = right;
            }
        }
        return point;
    };
    var connectionX = function (line) {
        var point = null;
        for (var i = 0; i < _bottomCuts.length; i++) {
            var bottom = _bottomCuts[i];
            if (line.y1 == bottom.y1 && line.x2 >= bottom.x1 && line.x2 <= bottom.x2 && line != bottom && !line.connected) {
                point = bottom;
            }
        }
        return point;
    };
    var canConnect = function (_cuts, type) {
        for (var i = 0; i < _cuts.length; i++) {
            var cut = _cuts[i];
            var point = type === 'x' ? connectionX(cut) : connectionY(cut);
            if (point) {
                return true;
            }
        }
        return false;
    };
    var connected = 0;
    // вертикальные пилы соединение линий
    while (canConnect(_rightCuts, 'y')) {
        connected = 0;
        for (var i = 0; i < _rightCuts.length; i++) {
            var cut = _rightCuts[i];
            var point = connectionY(cut);
            if (point) {
                _rightCuts[i].x1 = cut.x1;
                _rightCuts[i].y1 = cut.y1;
                _rightCuts[i].x2 = point.x2;
                _rightCuts[i].y2 = point.y2;
                if (!point.connected) {
                    point.connected = true;
                    connected++;
                }
            }
        }
        // удаление соединенных линий
        while (connected) {
            for (var i = 0; i < _rightCuts.length; i++) {
                if (_rightCuts[i].connected) {
                    _rightCuts.splice(i, 1);
                    connected--;
                    break;
                }
            }
        }
    }
    // горизонтальные пилы
    while (canConnect(_bottomCuts, 'x')) {
        connected = 0;
        for (var i = 0; i < _bottomCuts.length; i++) {
            var cut = _bottomCuts[i];
            var point = connectionX(cut);
            if (point) {
                _bottomCuts[i].x1 = cut.x1;
                _bottomCuts[i].y1 = cut.y1;
                _bottomCuts[i].x2 = point.x2;
                _bottomCuts[i].y2 = point.y2;
                if (!point.connected) {
                    point.connected = true;
                    connected++;
                }
            }
        }
        // удаление соединенных линий
        while (connected) {
            for (var i = 0; i < _bottomCuts.length; i++) {
                if (_bottomCuts[i].connected) {
                    _bottomCuts.splice(i, 1);
                    connected--;
                    break;
                }
            }
        }
    }
    cuts = _rightCuts.concat(_bottomCuts);
    return cuts;
};

var Figures2 = function (plywood) {
    var pWidth = plywood.getWidth();
    var pHeight = plywood.getHeight();
    var figures = [];
    var structure = [];
    var viewport = {
        width: pWidth,
        height: pHeight
    };
    plywood.figures.each(function (figure) {
        var width = figure.get('width');
        var height = figure.get('height');
        // todo: добавить валидацию
        if (width > viewport.width || height > viewport.height) return false;
        for (var i = 0; i < figure.get('count'); i++) {
            figures.push({
                width: width,
                height: height,
                x: 0,
                y: 0
            });
        }
    });
    figures.sort(function (figA, figB) {
        if (figA.height > figB.height) return -1;
        if (figA.height < figB.height) return 1;
        return 0;
    });
    var _figures = figures.slice();
    var _plywood = {
        width: pWidth,
        height: pHeight,
        sections: [{
            width: pWidth,
            height: pHeight,
            x: 0,
            y: 0,
            figure: null
        }]
    };
    structure.push(_plywood);
    var getSection = function (figure) {
        var sectionIndex = -1;
        var _perfectSection = _plywood.sections[0];
        for (var i = 0; i < _plywood.sections.length; i++) {
            var section = _plywood.sections[i];
            var sectionArea = section.width * section.height;
            var _perfectSectionArea = _perfectSection.width * _perfectSection.height;
            if (section.figure == null && sectionArea && sectionArea <= _perfectSectionArea) {
                if (figure.width <= section.width && figure.height <= section.height) {
                    _perfectSection = section;
                    sectionIndex = i;
                } else if (figure.height <= section.width && figure.width <= section.height) {
                    var buffer = figure.width;
                    figure.width = figure.height;
                    figure.height = buffer;
                    _perfectSection = section;
                    sectionIndex = i;
                }
            }
        }
        return sectionIndex;
    };
    while (_figures.length) {
        for (var i = 0; i < _figures.length; i++) {
            var el = _figures[i];
            var sectionIndex = getSection(el);
            var section = _plywood.sections[sectionIndex];
            if (sectionIndex > -1) {
                section.figure = el;
                _plywood.sections.push({
                    width: section.width - el.width,
                    height: el.height,
                    x: section.x + el.width,
                    y: section.y,
                    figure: null
                });
                _plywood.sections.push({
                    width: section.width,
                    height: section.height - el.height,
                    x: section.x,
                    y: section.y + el.height,
                    figure: null
                });
                _figures.splice(i, 1);
                break;
            } else {
                _plywood = {
                    width: pWidth,
                    height: pHeight,
                    sections: [{
                        width: pWidth,
                        height: pHeight,
                        x: 0,
                        y: 0,
                        figure: null
                    }]
                };
                structure.push(_plywood);
            }
        }
    }
    return structure;
};

module.exports = {
    Plywoods: Plywoods,
    Figures: Figures2,
    Cuts: Cuts
};
