var $ = require('jquery');
var Plywoods = require('../collections/plywoods');
var PlywoodView = require('./plywood');

var PlywoodHelper = require('../helpers/plywood');
var DrawHelper = require('../helpers/draw');

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

module.exports = Backbone.View.extend({

    el: '[data-js="plywood-cutter"]',

    events: {
        'click [data-js="new-plywood"]': 'onAddPlywood',
        'click [data-js="toggle-plywood-cut"]': 'togglePlywoodCut',
        'click [data-js="reset-plywood"]': 'onResetPlywood',
        'click [data-js="order"]': 'onOrder'
    },

    initialize: function (options) {
        options || (options = {});

        this.plywoods = options.plywoods;

        this.ui = {
            plywoodsList: this.$el.find('[data-js="plywoods-list"]'),
            toggleCut: this.$el.find('[data-js="toggle-plywood-cut"]'),
            plywoodsCut: this.$el.find('[data-js="plywood-cut"]'),
            plywoodReset: this.$el.find('[data-js="reset-plywood"]')
        };

        this.render();

        this.listenTo(this.plywoods, 'reset remove', this.render.bind(this));
        this.listenTo(Backbone.Events, 'PlywoodHelper:ready', this.render.bind(this));
        this.listenTo(Backbone.Events, 'plywood:change', function () {
            if (this.ui.toggleCut.hasClass('active')) {
                this.renderCut();
            }
        }.bind(this));
    },

    render: function () {
        this.ui.plywoodsList.html('');
        this.plywoods.each(function (plywood) {
            this.ui.plywoodsList.append(new PlywoodView({ plywood: plywood }).render());
        }.bind(this));
    },

    onAddPlywood: function (ev) {
        ev.preventDefault();

        this.plywoods.add({
            amount: 0
        });

        this.render();

        Backbone.Events.trigger('plywood:reset');
    },

    togglePlywoodCut: function () {
        if (this.ui.toggleCut.hasClass('active')) {
            this.ui.toggleCut.text('Показать схему распила');
            this.ui.plywoodsCut.html('');
        } else {
            this.ui.toggleCut.text('Спрятать схему распила');
            this.renderCut();
        }
        this.ui.toggleCut.toggleClass('active');
    },

    onResetPlywood: function (ev) {
        ev.preventDefault();

        this.plywoods.reset([{
            mark: 1
        }]);

        this.ui.toggleCut.text('Показать схему распила');
        this.ui.plywoodsCut.html('');
        this.ui.toggleCut.removeClass('active');
    },

    onOrder: function (ev) {
        ev.preventDefault();

        var data = [];
        var figures = [];

        this.plywoods.each(function (plywood) {
            var structure = DrawHelper.Figures(plywood);
            data.push({
                pid: plywood.businessData.id,
                col: structure.length
            });
            if (plywood.get('trimming')) {
                figures.push([plywood.get('number'), plywood.getWidth(), plywood.getHeight(), 1, 'trimming'].join('x'));
            } else {
                plywood.figures.each(function (figure) {
                    figures.push([plywood.get('number'), figure.get('width'), figure.get('height'), figure.get('count')].join('x'));
                });
            }
        });

        $.ajax({
            url: '/basket/addarray',
            method: 'post',
            data: {
                data: data
            },
            success: function () {
                setCookie('plywoodCutPrice', Number(this.$el.find('[data-js="cut-price"]').text()), { path: '/' });
                setCookie('plywoodFigures', figures.join(','), { path: '/' });
                window.location.href = '/basket';
            }.bind(this)
        });
    },

    renderCut: function () {
        var structure = DrawHelper.Plywoods(this.plywoods);
        var ratio = 5;
        var canvas;
        var context;
        this.ui.plywoodsCut.html('');
        for (var i = 0; i < structure.length; i++) {
            var plywood = structure[i];
            var cuts = DrawHelper.Cuts(plywood);
            canvas = document.createElement('canvas');
            canvas.className = 'm-plywoodCutter__canvas';
            canvas.width = plywood.width / ratio;
            canvas.height = plywood.height / ratio;
            context = canvas.getContext('2d');
            this.ui.plywoodsCut.append(canvas);
            for (var j = 0; j < plywood.sections.length; j++) {
                var section = plywood.sections[j];
                var x = Math.floor(section.x / ratio);
                var y = Math.floor(section.y / ratio);
                var width = Math.floor(section.width / ratio);
                var height = Math.floor(section.height / ratio);
                context.fillStyle = "#F5F5F5";
                context.fillRect(x, y, width, height);
                if (section.figure) {
                    var figureWidth = Math.floor(section.figure.width / ratio);
                    var figureHeight = Math.floor(section.figure.height / ratio);
                    context.fillStyle = "white";
                    context.fillRect(x, y, figureWidth, figureHeight);
                }
            }
            context.strokeStyle = "#EA4335";
            context.lineWidth = 2;
            for (var c = 0; c < cuts.length; c++) {
                context.beginPath();
                context.moveTo(cuts[c].x1 / ratio, cuts[c].y1 / ratio);
                context.lineTo(cuts[c].x2 / ratio, cuts[c].y2 / ratio);
                context.stroke();
            }
        }
    }

});
