var Row = require('./table-row');

module.exports = Backbone.View.extend({

    el: '[data-js="plywood-table"]',

    initialize: function (options) {
        options || (options = {});
        this.plywoods = options.plywoods;

        this.ui = {
            rows: this.$el.find('[data-js="plywood-table-rows"]'),
            materialPrice: this.$el.find('[data-js="material-price"]'),
            cutPrice: this.$el.find('[data-js="cut-price"]'),
            totalPrice: this.$el.find('[data-js="total-price"]'),
            delivery: this.$el.find('[data-js="plywood-delivery"]'),
            profit: this.$el.find('[data-js="plywood-profit"]')
        };

        this.render();

        this.listenTo(this.plywoods, 'add remove', function () {
            this.render();
            this.updatePrice();
        }.bind(this));
        this.listenTo(Backbone.Events, 'plywood:price', this.updatePrice.bind(this));
        this.listenTo(Backbone.Events, 'plywood:change', this.updatePrice.bind(this));
        this.listenTo(Backbone.Events, 'PlywoodBusinessData:ready', this.updatePrice.bind(this));
        this.listenTo(this.plywoods, 'reset', this.render.bind(this));
        this.listenTo(Backbone.Events, 'plywood:change', this.render.bind(this));
        this.listenTo(Backbone.Events, 'PlywoodHelper:ready', this.render.bind(this));
        this.listenTo(Backbone.Events, 'PlywoodBusinessData:ready', this.render.bind(this));
    },

    render: function () {
        this.ui.rows.html('');

        this.plywoods.each(function (plywood) {
            this.ui.rows.append(new Row({ plywood: plywood, structure: this.plywoods.structure }).render());
        }.bind(this));
    },

    updatePrice: function () {
        var material = this.plywoods.calcMaterial();
        var cut = this.plywoods.calcCut();
        var unloading = this.plywoods.calcUnloading();
        var volume = Math.ceil(this.plywoods.calcVolume() / 1000000000);
        var pallet = volume * 400;
        var delivery = Number(this.ui.delivery.val());
        var profit = Number(this.ui.profit.val());

        this.ui.materialPrice.text(material);
        this.ui.cutPrice.text(cut ? ((cut + unloading + pallet + delivery) * profit * 100) / 100 : 0);
        if (this.plywoods.hasFigures()) {
            this.ui.totalPrice.text(Math.round((material + cut + unloading + pallet + delivery) * profit * 100) / 100);
        } else {
            this.ui.totalPrice.text(0);
        }
    }

});
