module.exports = Backbone.View.extend({

    className: 'm-plywoodCutter__figure',

    template: _.template($('[data-js="template-figure"]').html()),

    events: {
        'keyup [data-js="width"]': 'onChangeWidth',
        'keyup [data-js="height"]': 'onChangeHeight',
        'keyup [data-js="count"]': 'onChangeCount',
        'click [data-js="remove-figure"]': 'onRemoveFigure'
    },

    initialize: function (options) {
        options || (options = {});
        this.figure = options.figure;
    },

    render: function () {
        this.$el.html(this.template(this.figure.toJSON()));
        return this.el;
    },

    onChangeWidth: function (ev) {
        var value = parseFloat(ev.target.value.replace(',', '.')) || 1;
        this.figure.set('width', value);
    },

    onChangeHeight: function (ev) {
        var value = parseFloat(ev.target.value.replace(',', '.')) || 1;
        this.figure.set('height', value);
    },

    onChangeCount: function (ev) {
        var value = Number(ev.target.value) || 1;
        this.figure.set('count', value);
    },

    onRemoveFigure: function (ev) {
        ev.preventDefault();
        this.figure.destroy();
    }

});
