module.exports = Backbone.View.extend({

    tagName: 'tr',

    template: _.template($('[data-js="template-plywood-row"]').html()),

    initialize: function (options) {
        options || (options = {});
        this.plywood = options.plywood;
        this.structure = options.structure;

        this.listenTo(this.plywood, 'change', function () {
            this.render();
            Backbone.Events.trigger('plywood:price');
        }.bind(this));
    },

    render: function () {
        this.$el.html(this.template({
            name: this.plywood.getName(),
            meterPrice: this.plywood.getMeterPrice(),
            figuresMeter: this.plywood.getFiguresMeter(),
            weight: this.plywood.getWeight(),
            itemPrice: this.plywood.getItemPrice(),
            count: this.plywood.getCount(),
            amount: this.plywood.getAmount()
        }));
        return this.el;
    }

});
