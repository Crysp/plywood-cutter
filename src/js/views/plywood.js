var FigureView = require('./figure');

var PlywoodHelper = require('../helpers/plywood');

module.exports = Backbone.View.extend({

    template: _.template($('[data-js="template-plywood"]').html()),

    events: {
        'click [data-js="add-figure"]': 'onAddFigure',
        'click [data-js="trimming"]': 'onSelectTrimming',
        'click [data-js="remove-plywood"]': 'onRemovePlywood'
    },

    initialize: function (options) {
        options || (options = {});
        this.plywood = options.plywood;
        this.ui = {};

        this.listenTo(this.plywood.figures, 'add remove reset', this.renderFigures.bind(this));
        this.listenTo(this.plywood, 'change', this.render.bind(this));
    },

    render: function () {
        this.$el.html(this.template(this.plywood.toJSON()));
        this.bindUI();
        this.renderTypes();
        this.renderFigures();
        return this.el;
    },

    renderTypes: function () {
        if (!PlywoodHelper.synced()) return;
        var _mark = PlywoodHelper.marks.get(this.plywood.get('type'));
        var _kind = _mark.kinds.get(this.plywood.get('kind')) || _mark.kinds.models[0];
        var _format = _mark.formats.get(this.plywood.get('format')) || _mark.formats.models[0];
        var _depth = _mark.depths.get(this.plywood.get('depth')) || _mark.depths.models[0];

        this.ui.plywoodMark.html('');
        this.ui.plywoodKind.html('');
        this.ui.plywoodFormat.html('');
        this.ui.plywoodDepth.html('');

        PlywoodHelper.marks.each(function (mark) {
            this.ui.plywoodMark.append('<option value="' + mark.id + '" ' + (mark.id == _mark.id ? 'selected' : '') + '>' + mark.getName() + '</option>');
        }.bind(this));

        _mark.kinds.each(function (kind) {
            this.ui.plywoodKind.append('<option value="' + kind.id + '" ' + (kind.id == _kind.id ? 'selected' : '') + '>' + kind.getName() + ' сорт</option>');
        }.bind(this));

        _mark.formats.each(function (format) {
            this.ui.plywoodFormat.append('<option value="' + format.id + '" ' + (format.id == _format.id ? 'selected' : '') + '>' + format.getName() + '</option>');
        }.bind(this));

        _mark.depths.each(function (depth) {
            this.ui.plywoodDepth.append('<option value="' + depth.id + '" ' + (depth.id == _depth.id ? 'selected' : '') + '>' + depth.getName() + ' мм</option>');
        }.bind(this));
    },

    renderFigures: function () {
        this.ui.figuresList.html('');

        this.plywood.figures.each(function (figure) {
            this.ui.figuresList.append(new FigureView({ figure: figure }).render());
        }.bind(this));
    },

    bindUI: function () {
        this.ui.addFigure = this.$el.find('[data-js="add-figure"]');
        this.ui.figureWidth = this.$el.find('[data-js="figure-width"]');
        this.ui.figureHeight = this.$el.find('[data-js="figure-height"]');
        this.ui.figureCount = this.$el.find('[data-js="figure-count"]');

        this.ui.figuresList = this.$el.find('[data-js="figures-list"]');

        this.ui.plywoodDepth = this.$el.find('[data-js="plywood-depth"]');
        this.ui.plywoodKind = this.$el.find('[data-js="plywood-kind"]');
        this.ui.plywoodFormat = this.$el.find('[data-js="plywood-format"]');
        this.ui.plywoodMark = this.$el.find('[data-js="plywood-mark"]');

        this.ui.removePlywood = this.$el.find('[data-js="remove-plywood"]');

        this.bindEvents();
    },

    bindEvents: function () {
        this.ui.figureWidth.on('keyup', function (ev) {
            var key = ev.keyCode || ev.which;
            if (key === 13) {
                this.onAddFigure(ev);
            }
        }.bind(this));
        this.ui.figureHeight.on('keyup', function (ev) {
            var key = ev.keyCode || ev.which;
            if (key === 13) {
                this.onAddFigure(ev);
            }
        }.bind(this));
        this.ui.figureCount.on('keyup', function (ev) {
            var key = ev.keyCode || ev.which;
            if (key === 13) {
                this.onAddFigure(ev);
            }
        }.bind(this));

        this.ui.plywoodMark.on('change', function (ev) {
            this.plywood.set('type', ev.currentTarget.value);
            this.renderTypes();
            this.plywood.fetchBusinessData();
        }.bind(this));

        this.ui.plywoodFormat.on('change', function (ev) {
            this.plywood.set('format', ev.currentTarget.value);
            this.plywood.fetchBusinessData();
        }.bind(this));

        this.ui.plywoodDepth.on('change', function (ev) {
            this.plywood.set('depth', ev.currentTarget.value);
            this.plywood.fetchBusinessData();
        }.bind(this));

        this.ui.plywoodKind.on('change', function (ev) {
            this.plywood.set('kind', ev.currentTarget.value);
            this.plywood.fetchBusinessData();
        }.bind(this));
    },

    onAddFigure: function (ev) {
        ev.preventDefault();

        var width = parseFloat(this.ui.figureWidth.val()) || 0;
        var height = parseFloat(this.ui.figureHeight.val()) || 0;
        var count = parseFloat(this.ui.figureCount.val()) || 0;

        if (!width || !height || !count) return;

        this.plywood.figures.add({
            width: width,
            height: height,
            count: count
        });

        this.ui.figureWidth.val('');
        this.ui.figureHeight.val('');
        this.ui.figureCount.val('');

        this.ui.figureWidth.focus();

        this.renderFigures();
    },

    onSelectTrimming: function (ev) {
        this.plywood.setTrimming($(ev.target).is(':checked'));
    },

    onRemovePlywood: function (ev) {
        ev.preventDefault();
        this.plywood.destroy();
    }

});
