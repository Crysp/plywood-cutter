var Plywoods = require('./collections/plywoods');
var PlywoodView = require('./views/index');
var PlywoodTableView = require('./views/table');

$(function () {

    var plywoods = new Plywoods([{
        mark: 1
    }]);
    var plywoodView = new PlywoodView({ plywoods: plywoods });
    var plywoodTableView = new PlywoodTableView({ plywoods: plywoods });

});
