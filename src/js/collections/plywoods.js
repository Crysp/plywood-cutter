var Plywood = require('../models/plywood');

var PlywoodHelper = require('../helpers/plywood');
var DrawHelper = require('../helpers/draw');

module.exports = Backbone.Collection.extend({

    model: Plywood,

    initialize: function (models, options) {
        this.structure = DrawHelper.Plywoods(this);

        this.on('add remove', function() {
            this.each(function (plywood, index) {
                plywood.set('number', index + 1, { silent: true });
            });
        }.bind(this));
        this.listenTo(Backbone.Events, 'plywood:change', function () {
            this.structure = DrawHelper.Plywoods(this);
        }.bind(this));
    },

    hasFigures: function () {
        for (var i = 0; i < this.structure.length; i++) {
            if (this.structure[i].sections.length > 1) return true;
        }
        return false;
    },

    calcMaterial: function () {
        var sum = 0;
        this.each(function (plywood) {
            var _plywood = DrawHelper.Figures(plywood);
            sum += plywood.getItemPrice() * _plywood.length;
        });
        return sum;
    },

    calcCut: function () {
        var sum = 0;
        this.each(function (plywood) {
            sum += plywood.get('amount');
        });
        return sum;
    },

    calcUnloading: function () {
        var sum = 0;
        this.each(function (plywood) {
            var _plywood = DrawHelper.Figures(plywood);
            sum += plywood.getUnloadPrice() * _plywood.length;
        });
        return sum;
    },

    calcVolume: function () {
        var sum = 0;
        this.each(function (plywood) {
            var volume = 0;
            var _plywood = DrawHelper.Figures(plywood);
            var mark = PlywoodHelper.marks.get(plywood.get('type'));
            var depth = mark.depths.get(plywood.get('depth')) || mark.depths.models[0];
            for (var i = 0; i < _plywood.length; i++) {
                var __plywood = _plywood[i];
                for (var j = 0; j < __plywood.sections.length; j++) {
                    var section = __plywood.sections[j];
                    if (section.figure) {
                        volume += section.figure.width * section.figure.height * depth.get('value');
                    }
                }
            }
            sum += volume;
        });
        return sum;
    }

});
