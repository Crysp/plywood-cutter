var Figure = require('../models/figure');

module.exports = Backbone.Collection.extend({

    model: Figure,

    add: function (models, options) {
        var existed = false;
        if (_.isObject(models) && models.hasOwnProperty('width') && models.hasOwnProperty('height') && models.hasOwnProperty('count')) {
            existed = this.findWhere({ width: models.width, height: models.height });
            if (existed) {
                existed.set('count', existed.get('count') + models.count);
                return existed;
            }
        }
        return Backbone.Collection.prototype.add.call(this, models, options);
    }

});
