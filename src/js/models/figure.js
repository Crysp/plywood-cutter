module.exports = Backbone.Model.extend({

    defaults: {
        width: 0,
        height: 0,
        count: 0
    }

});
