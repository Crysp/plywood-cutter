module.exports = Backbone.Model.extend({

    idAttribute: 'name',

    defaults: {
        name: null
    },

    getName: function () {
        return this.get('name');
    }

});
