module.exports = Backbone.Model.extend({

    idAttribute: 'name',

    defaults: {
        width: 0,
        height: 0,
        name: null
    },

    initialize: function () {
        this.set('width', parseFloat(this.get('width').replace(',', '.')));
        this.set('height', parseFloat(this.get('height').replace(',', '.')));
        this.set('name', this.getName());
    },

    getName: function () {
        return this.get('width') + 'x' + this.get('height');
    }

});
