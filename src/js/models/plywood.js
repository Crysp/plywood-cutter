var Figures = require('../collections/figures');

var PlywoodHelper = require('../helpers/plywood');
var DrawHelper = require('../helpers/draw');

module.exports = Backbone.Model.extend({

    defaults: {
        number: 1,
        format: '1525x1525',
        kind: '1/2',
        depth: '3',
        amount: 0,
        trimming: false,
        type: 'fanera_fk'
    },

    initialize: function (attributes, options) {
        this.figures = new Figures;
        this.businessData = {
            "id": "0",
            "name": "Фанера ФК 3 мм сорт 1/2 Ш2 1525x1525",
            "price": 371,
            "weight": 4,
            "price_metr": 159.5,
            "sheet_weight": 0
        };
        this.fetchBusinessData();
        this.listenTo(this.figures, 'add remove', function (figure) {
            if (!PlywoodHelper.synced()) return;
            var mark = PlywoodHelper.marks.get(this.get('type'));
            var format = mark.formats.get(this.get('format')) || mark.formats.models[0];
            // если фигура не влезает на лист, удаляю ее
            if (
                (figure.get('width') > format.get('width') || figure.get('width') > format.get('height')) &&
                (figure.get('height') > format.get('height') || figure.get('height') > format.get('width'))
            ) {
                figure.destroy();
            } else {
                this.calcAmount();
            }
        }.bind(this));
        this.listenTo(this.figures, 'change', function () {
            this.trigger('change');
            Backbone.Events.trigger('plywood:change');
        }.bind(this));
        this.on('change', this.calcAmount.bind(this));
        this.on('change:type', function () {
            if (!PlywoodHelper.synced()) return;
            var _mark = PlywoodHelper.marks.get(this.get('type'));
            this.set({
                kind: _mark.kinds.models[0].getName(),
                format: _mark.formats.models[0].getName(),
                depth: _mark.depths.models[0].getName()
            });
            // todo пока нет валидации, удаляю все фигуры при смене листа
            this.figures.reset();
        }.bind(this));
    },

    fetchBusinessData: function () {
        $.ajax({
            url: '/api/getproduct',
            method: 'get',
            data: {
                type: this.get('type'),
                kind: this.get('kind'),
                thickness: this.get('depth'),
                format: this.get('format')
            },
            success: function (data) {
                this.businessData = data;
            }.bind(this),
            complete: function () {
                Backbone.Events.trigger('PlywoodBusinessData:ready');
            }.bind(this)
        });
    },

    setTrimming: function (isChecked) {
        this.set({ trimming: isChecked });
        Backbone.Events.trigger('plywood:change');
    },

    calcAmount: function () {
        if (!PlywoodHelper.synced()) return;
        var value = 0;
        var mark = PlywoodHelper.marks.get(this.get('type'));
        var depth = mark.depths.get(this.get('depth')) || mark.depths.models[0];
        var depthPrice = PlywoodHelper.marksStatic.get(this.get('type')).depths.get(this.get('depth')) || PlywoodHelper.marksStatic.get(this.get('type')).depths.models[0];
        var format = mark.formats.get(this.get('format')) || mark.formats.models[0];
        var plywood = DrawHelper.Figures(this);
        var runningMeter = this.getFiguresMeter();

        // выбор цены, которая зависит от погонного метра
        // todo: добавить в апи цену за погонный метр
        if (runningMeter <= 100) value = depthPrice.get('price')[0];
        else if (runningMeter > 100 && runningMeter <= 700) value = depthPrice.get('price')[1];
        else value = depthPrice.get('price')[2];

        this.set('amount', Math.round(runningMeter * value * 100) / 100);
        Backbone.Events.trigger('plywood:change');
    },

    calcCount: function () {
        return 0;
    },

    getName: function () {
        return this.businessData.name;
    },

    getMeterPrice: function () {
        return this.businessData.price_metr;
    },

    getFiguresMeter: function () {
        var count = 0;
        var plywoods = DrawHelper.Figures(this);
        for (var p = 0; p < plywoods.length; p++) {
            var cuts = DrawHelper.Cuts(plywoods[p]);
            for (var i = 0; i < cuts.length; i++) {
                var cut = cuts[i];
                if (cut.x1 == cut.x2) count += cut.y2 - cut.y1;
                else count += cut.x2 - cut.x1;
            }
        }
        if (count && this.get('trimming')) count += this.getWidth() * 2 + (this.getHeight('height') + 14) * 2;
        return Math.round(count / 1000 * 100) / 100;
    },

    /*
    * 1-2 104
    * 2-4 100
    * 4-5 114
    *
    * */

    getWeight: function () {
        if (!PlywoodHelper.synced()) return 0;
        var plywoods = DrawHelper.Figures(this);
        return this.businessData.sheet_weight * plywoods.length;
    },

    getItemPrice: function () {
        if (!PlywoodHelper.synced()) return 0;
        return this.businessData.price;
    },

    getUnloadPrice: function () {
        if (!PlywoodHelper.synced()) return 0;
        var wps = PlywoodHelper.wps.findWhere({ format: this.get('format'), depth: Number(this.get('depth')) });
        // todo: убрать форматы для которых нет цены в WPS
        return (wps && wps.get('price')) || 0;
    },

    getCount: function () {
        return DrawHelper.Figures(this).length;
    },

    getAmount: function () {
        return this.get('amount');
    },

    getWidth: function () {
        if (!PlywoodHelper.synced()) return 0;
        var mark = PlywoodHelper.marks.get(this.get('type'));
        var format = mark.formats.get(this.get('format')) || mark.formats.models[0];
        return this.get('trimming') ? (format.get('width') - 14) : format.get('width');
    },

    getHeight: function () {
        if (!PlywoodHelper.synced()) return 0;
        var mark = PlywoodHelper.marks.get(this.get('type'));
        var format = mark.formats.get(this.get('format')) || mark.formats.models[0];
        return this.get('trimming') ? (format.get('height') - 14) : format.get('height');
    }

});
