module.exports = Backbone.Model.extend({

    idAttribute: 'value',

    defaults: {
        value: 0
    },

    getName: function () {
        return this.get('value');
    }

});
