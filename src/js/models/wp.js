module.exports = Backbone.Model.extend({

    defaults: {
        format: 0,
        depth: 0,
        weight: 0,
        price: 0
    }

});
