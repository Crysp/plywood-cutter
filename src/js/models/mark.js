var Kinds = require('../collections/kinds');
var Formats = require('../collections/formats');
var Depths = require('../collections/depths');

module.exports = Backbone.Model.extend({

    idAttribute: 'type',

    defaults: {
        type: 'fanera_fb',
        name: null
    },

    url: function () {
        return '/api/getfilter?type=' + this.get('type');
    },

    parse: function (res) {
        this.formats.add(res.formats);
        for (var i = 0; i < res.depths.length; i++) {
            this.depths.add({ value: Number(res.depths[i]) });
        }
        for (var j = 0; j < res.kinds.length; j++) {
            this.kinds.add({ name: res.kinds[j] });
        }
        return res;
    },

    initialize: function (attributes, options) {
        options || (options = {});
        this.formats = new Formats;
        this.depths = new Depths;
        this.kinds = new Kinds;
    },

    getName: function () {
        return this.get('name');
    }

});
